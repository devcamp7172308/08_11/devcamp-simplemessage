const express = require("express");
const app = express();
const port = 8000;
let array = ["blue", "red", "green"];
app.get("/", (req, res) => {
  let today = new Date();
  let string = `Hello, today is ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`;
  res.json({
    mess: string,
  });
});

//Get Method:
app.get("/get-method", (req, res) => {
  res.json({
    Result_Get: array,
  });
});

//Post Method: (ADDING ELEMENT)
app.post("/post-method", (req, res) => {
  res.json({
    Resust_Post: (array = [...array, "pink"]),
  });
});

//Put Method:
app.put("/put-method", (req, res) => {
  array[2] = "yellow";
  res.json({
    Result_Put: array,
  });
});

//Delete Method:
app.delete("/delete-method", (req, res) => {
  array.shift();
  res.json({
    Result_Delete: array,
  });
});
//Test
//Listen on Port:
app.listen(port, () => {
  console.log(`Apps running on port ${port}`);
});
